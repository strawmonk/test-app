<?php
namespace Tests\AppBundle\Adapters;

use AppBundle\Entity\Repositories\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RestaurantRepositoryTest extends WebTestCase
{
    public function testFind()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $repo = $container->get(RestaurantRepository::class);
        $result = $repo->find(["borough" => "Manhattan"]);

        $this->assertEquals("select * from restaurants where borough='Manhattan'", $result);
    }
}