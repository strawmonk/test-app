<?php
namespace Tests\AppBundle\Adapters;

use AppBundle\Adapters\SqlAdapter;
use PHPUnit\Framework\TestCase;

class SqlAdapterTest extends TestCase
{
    private $cases = [
        [
            'query' => ["borough" => "Manhattan"],
            'sql' => "select * from restaurants where borough='Manhattan'"
        ],
        [
            'query' => ["cuisine" => "Italian", "zipcode" => "10075"],
            'sql' => "select * from restaurants where cuisine='Italian' and zipcode='10075'"
        ],
        [
            'query' => ["grades_score" => ['$gt' => 30]],
            'sql' => "select * from restaurants where grades_score > 30"
        ],
        [
            'query' => ["grades_score" => ['$lt' => 30]],
            'sql' => "select * from restaurants where grades_score < 30"
        ],
        [
            'query' => ['$or' => ["cuisine" => "Italian", "zipcode" => "10075"]],
            'sql' => "select * from restaurants where cuisine='Italian' or zipcode='10075'"
        ]
    ];

    public function testFind()
    {
        $adapter = new SqlAdapter();
        $adapter->setTable('restaurants');

        foreach ($this->cases as $case) {
            $this->assertEquals(
                $case['sql'],
                $adapter ->find($case['query'])
            );
        }

    }
}