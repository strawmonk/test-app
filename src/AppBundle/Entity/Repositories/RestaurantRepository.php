<?php
namespace AppBundle\Entity\Repositories;

use AppBundle\Adapters\SqlAdapterInterface;
use AppBundle\Entity\Restaurant;

class RestaurantRepository
{
    protected $adapter;

    public function __construct(SqlAdapterInterface $adapter)
    {
        $adapter->setTable(Restaurant::TABLE);
        $this->adapter = $adapter;
    }

    public function find(array $criteria): string
    {
        return $this->adapter->find($criteria);
    }
}