<?php
namespace AppBundle\Adapters;

interface SqlAdapterInterface
{
    public function find(array $criteria): string;
    public function setTable(string $table);
}