<?php
namespace AppBundle\Adapters;


class SqlAdapter implements SqlAdapterInterface
{
    private $table = null;

    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param array $criteria
     * @return string
     */
    public function find(array $criteria): string
    {
        if(!$this->table)
            throw new \InvalidArgumentException('table name cannot be null');

        return "select * from {$this->table} where {$this->criteriaToSql($criteria)}";
    }

    /**
     * @param array $criteria
     * @param string $glue
     * @return string
     */
    private function criteriaToSql(array $criteria, string $glue = ' and '): string
    {
        $whereSql = '';
        foreach ($criteria as $key => $value){
            if($key == '$or' && is_array($value)){
                $whereSql = $this->criteriaToSql($value, ' or ');
            } elseif(is_array($value)){
                if(in_array(key($value), ['$gt', '$lt'])){
                    $sign = key($value) == '$gt' ? '>' : '<';
                    $gtValue = current($value);
                    $whereSql .= "{$key} {$sign} {$gtValue}";
                }
            }else{
                $whereSql .= "{$key}='$value'";
            }

            if($value !== end($criteria))
                $whereSql .= $glue;
        }

        return $whereSql;
    }

}